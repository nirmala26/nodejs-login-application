const crypto = require('crypto');
const algorithm = 'aes-256-cbc';
function decrypt(text) {
    var key = "123|a123123123123123@&";
    var decipher = crypto.createDecipher('aes-256-cbc', key);
    var decrypted = decipher.update(text, 'hex', 'utf-8');
    decrypted += decipher.final('utf-8');
    return decrypted;
}

async function login(req, res) {
    try {

        let error = "";
        if (!req.body || !req.body.username || !req.body.password) {
            error = "username or password is not valid";
        }
        if (error) {
            res.render('error', {
                error: error
            });
            return;
        }
        let usernameQuery = "SELECT * FROM `users` WHERE user_name = '" + req.body.username + "'";

        __db.query(usernameQuery, (err, result) => {
            if (err) {
                res.render('error', {
                    error: err.message
                });
                return;
            } else {


                if (result && result.length > 0) {
                    let dbPwd = "";
                    Object.keys(result).forEach(function (key) {
                        var row = result[key];
                        dbPwd = row.password;
                    });
                    try {
                        dbPwd = decrypt(dbPwd);
                    } catch (E) {
                        res.render('error', {
                            error: "wrong credentials"
                        });
                        return;
                       
                    }
                    if (dbPwd === req.body.password) {
                        res.render('success', {
                            message: `welcome ${req.body.username}`
                        });
                        return;
                    } else {
                        res.render('error', {
                            error: "wrong credentials"
                        });
                        return;
                    }



                } else {
                    res.render('register', {
                        message: "user not available",
                        qs: req.query
                    });
                    return;
                }
            }
        });

    } catch (E) {
        res.render('error', {
            error: E.message
        });
        return;

    }



}

async function render(req, res) {
    res.render('login', {
        qs: req.query
    });
}

module.exports = {
    login,
    render
}