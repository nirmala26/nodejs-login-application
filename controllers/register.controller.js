const crypto = require('crypto');
const algorithm = 'aes-256-cbc';

function encrypt(text){
    var key = "123|a123123123123123@&";
    var cipher = crypto.createCipher('aes-256-cbc', key);
    var crypted = cipher.update(text, 'utf-8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
  }

async function register(req, res) {
    try {
        let error = "";
        if (!req.body || !req.body.username || !req.body.password) {
            error = "username or password is not valid";
        }

        if (error) {
            res.render('error', {
                error: error
            });
            return;
        }
        function checkUser(query) {
            return new Promise(async (resolve, reject) => {
                try {

                    __db.query(query, (err, result) => {
                        if (err) {
                            reject(E);
                        } else {
                            if (result && result.length > 0) {
                                resolve(true);
                            } else {
                                resolve(false);
                            }
                        }
                    })
                } catch (E) {
                    reject(E);
                    return;
                }
            })

        }
        let usernameQuery = "SELECT * FROM `users` WHERE user_name = '" + req.body.username + "'";
        let check = await checkUser(usernameQuery);
        if (check) {
            res.render('error', {
                error: 'user already registered'
            });
            return;
        }
        let encryptedPwd = encrypt(req.body.password.toString());
        let query = "INSERT INTO `users` (email,password,user_name) VALUES ('" + (req.body.email = req.body.email ? req.body.email : "") + "', '" + encryptedPwd + "', '" + req.body.username + "')";
        __db.query(query, (err, result) => {
            if (err) {
                res.render('error', {
                    error: err.message
                });
                return;
            } else {
                res.render('success', {
                    message: "Registered successfully"
                });
                return;
            }
        });

    } catch (E) {
        res.render('error', {
            error: E.message
        });
        return;
    }
}

async function render(req, res) {
    res.render('register', {
        qs: req.query,
        "message": ""
    });
}

module.exports = {
    register,
    render
}