var express = require('express');
var bodyParser = require('body-parser');
var mysql = require('mysql');
const path = require('path');
const config = require('./config.js');
const app = express();
var loginRouter = require('./routes/login.js');
var registerRouter = require('./routes/register.js');



(async function () {
    try {
        const db = mysql.createConnection({
            host: config.sql_host,
            user: config.sql_username,
            password: config.sql_password,
            database: config.database
        });

        await connectDb();
        console.log("connected to database")
        global.__db = db;
        app.set('views', __dirname + '/views');
        app.set('view engine', 'ejs'); // configure template engine
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json()); // parse form data client
        app.use(express.static(path.join(__dirname, 'public'))); // configure express to use public folder

        app.use('/', loginRouter);
        app.use('/register', registerRouter);


        app.listen(config.SERVER_PORT, () => {
            console.log(`Server running on port: ${config.SERVER_PORT}`);
        });

        function connectDb() {
            return new Promise(async (resolve, reject) => {
                try {
                    db.connect((err) => {
                        if (err) {
                            reject(err);
                            return;
                        } else {
                            resolve(true);
                        }

                    });
                } catch (E) {
                    reject(E);

                }
            });
        }
    } catch (E) {
        console.log(E.message);
    }

})();

