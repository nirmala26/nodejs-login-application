var express = require('express');
var router = express.Router();
var controller =  require('../controllers/login.controller');


router.get('/',controller.render);
router.post('/login',controller.login);
module.exports = router;
