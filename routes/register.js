var express = require('express');
var router = express.Router();
var controller = require('../controllers/register.controller');


router.post('/', controller.register);
router.get('/view', controller.render);
module.exports = router;
